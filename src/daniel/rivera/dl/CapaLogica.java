package daniel.rivera.dl;
import daniel.rivera.bl.Camisa;
import daniel.rivera.bl.Catalogo;
import daniel.rivera.bl.Cliente;
import java.util.ArrayList;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class CapaLogica {
    private ArrayList<Catalogo> catalogos;
    private ArrayList<Camisa> camisas;
    private ArrayList<Cliente> clientes;

    /**
     * Contructor que es utilizado para inicializar los arreglos.
     */
    public CapaLogica() {
        catalogos = new ArrayList<>();
        camisas = new ArrayList<>();
        clientes = new ArrayList<>();
    }// FIN DE CONTRUCTOR.


    /**
     * Metodo que recorre el nuevo arreglo de catalogos.
     *
     * @return devuelve la información del arreglo de String.
     */
    public String[] getCatalogo() {
        int cont = 0;
        String[] info = new String[catalogos.size()];
        for (Catalogo resgisCat : catalogos) {
            info[cont] = resgisCat.toString();
            cont++;
        }
        return info;
    }// FIN DE GETCATALOGO.


    public Catalogo averiguarCatalogo(String Id) {
        for (Catalogo  catalogo : catalogos) {
            if (Id.equals(catalogo.getId())) {
                return catalogo;
            }
        }
        return null;
    }

    /**
     * Metodo que recorre el nuevo arreglo de camisa.
     *
     * @return devuelve la información de el arreglo getCamisa de tipo  arreglo de String.
     */
    public String[] getCamisa() {
        int cont = 0;
        String[] info = new String[camisas.size()];
        for (Camisa rCamisa : camisas) {
            info[cont] = rCamisa.toString();
            cont++;
        }
        return info;
    }// FIN DE GETCAMISA.


    public Cliente averiguarCliente(String cedula) {
        for (Cliente cliente : clientes) {
            if (cedula.equals(cliente.getCedula())) {
                return cliente;
            }
        }
        return null;
    }
    public Camisa averiguarCamisa(String id) {
        for (Camisa  cam : camisas) {
            if (id.equals(cam.getid())) {
                return cam;
            }
        }
        return null;
    }

    /**
     * Metodo que recorre el nuevo arreglo de cliente.
     *
     * @return devuelve la información de el arreglo getCliente  de tipo  arreglo de String.
     */
    public String[] getCliente() {
        int cont = 0;
        String[] info = new String[clientes.size()];
        for (Cliente rCliente : clientes) {
            info[cont] = rCliente.toString();
            cont++;
        }
        return info;
    }// FIN DE GETCLIENTE.

    /**
     *metodo utilizado para validar la  información del cliente, para que nada se repita.
     * @param Pnombre: variable de la clase Cliente de tipo String en el cual se valida el nombre del cliente ingresado por el usuario.
     * @param Papellido1: variable de la clase Cliente de tipo String en el cual se valida el primer apellido del cliente ingresado por el usuario.
     * @param Papellido2: variable de la clase Cliente de tipo String en el cual se valida el segundo apellido  del cliente ingresado por el usuario.
     * @param PdirreccionExacta:dato referente a la  direción en donde el usuario desea recibir su pedido es de tipo String.
     * @param Pcorreo: variable de la clase Cliente en donde se valida el correo de la persona quien hace el pedido, es de tipo String.
     * @return retorna si un dato se repite o no.
     */
    public String validarCliente(String Pnombre, String Papellido1, String Papellido2, String PdirreccionExacta, String Pcorreo) {

        for (Cliente rCliente : clientes) {
            if (Pcorreo.equals(rCliente.getCorreo()) || Papellido1.equals(rCliente.getApellido1()) || Papellido2.equals(rCliente.getApellido2()) || PdirreccionExacta.equals(rCliente.getDirreccionExacta()) || Pnombre.equals(rCliente.getNombre())) {
                return rCliente.getCorreo();
            } else if (Papellido1.equals(rCliente.getApellido1())) {
                return rCliente.getApellido1();
            } else if (Papellido2.equals(rCliente.getApellido2())) {
                return rCliente.getApellido2();
            } else if (PdirreccionExacta.equals(rCliente.getDirreccionExacta())) {
                return rCliente.getDirreccionExacta();
            } else if (Pnombre.equals(rCliente.getNombre())) ;
            return rCliente.getNombre();
        }//Fin
        return null;
    }
    public int buscarPosCatalogo(String id) {
        int posicion = 0;
        for (Catalogo cat : catalogos) {
            if (id.equals(cat.getId())) {
                return posicion;
            }
            posicion++;
        }
        return -1;
    }
    public String registrarCliente(Cliente client) {
        clientes.add(client);
        return null;
    }
    public String registrarCamisa(Camisa cam) {
        camisas.add(cam);
        return null;
    }
    public String registrarCatalogo(Catalogo cat) {
        catalogos.add(cat);
        return null;
    }
    public String[] imprimirCatalogo() {
        String[] info = new String[catalogos.size()];
        for (int i = 0; i < catalogos.size(); i++) {
            info[i] = catalogos.get(i).toString();
        }
        return info;
    }
    public String[] imprimirCliente() {
        String[] info = new String[clientes.size()];
        for (int i = 0; i < clientes.size(); i++) {
            info[i] = clientes.get(i).toString();
        }
        return info;
    }

    public String[] imprimirCamisa() {
        String[] info = new String[camisas.size()];
        for (int i = 0; i < camisas.size(); i++) {
            info[i] = camisas.get(i).toString();
        }
        return info;
    }

}// FIN DE CLASS.