package daniel.rivera.tl;

import daniel.rivera.bl.Camisa;
import daniel.rivera.bl.Catalogo;
import daniel.rivera.bl.Cliente;
import daniel.rivera.dl.CapaLogica;
import java.time.LocalDate;


/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Controller {
    private CapaLogica logica;

    /**
     * Constructor de la capa logica, se inicializa la acapa logica para que esta pueda ser sub llamada.
     */
    public Controller() {
        logica = new CapaLogica();
    }

    /**
     * meteodo en donde se incializa el objeto catalogo y se manda a que se almacene en el ArrayList, llamando la capa logica
     *
     //TODO* @param
     * @param id :              dato identificativo del catálogo, hace referencia a un codigo único del mismo.
     * @param nombreDelMeS :    variable en donde se almacena el nombre del mes de creación de dicho catálogo.
     * @param fechaDeCreacion : dato referente a la fecha de creación del catálogo ingresado por el usuario; es de tipo String.
     * @return
     */

    public String registrarCatalogo(String id, String nombreDelMeS, LocalDate fechaDeCreacion) {
        Catalogo ct = new Catalogo(id,nombreDelMeS,fechaDeCreacion);
        logica.registrarCatalogo(ct);
        return id;
    }// FIN DE REGISTAR CATALOGO

    /**
     * metodod usado para imprimir el arrayList de catalogo.
     *
     * @return retorna la lista de catalogos.
     */
    public String[] ListarCatalogos() {
        return logica.getCatalogo();
    }// FIN DE LISTAR CATALOGO.

    /**
     * meteodo en donde se incializa el objeto camisa y se manda a que se almacene en el ArrayList, llamando la capa logica
     *
     * @param tam         :      dato de la clase Camisa de tipo String en el cual se guara el tamaño de la creación de la camisa ingresado por el usuario.
     * @param descri      :   variable que describe detalladamente las caracteristicas de la camisa.
     * @param color       :    variable en donde se guarda el color de la camisa deseada por el cliente.
     * @param id          :dato   identificativo de la camisa, hace referencia a un codigo único de la mismo.
     * @param precio      :   variable que contiene el precio despectivo de la camisa en especifico ingresado por el usuario
     */
    public String registrarCamisa(String tam, String descri, String color, String id, double precio  ) {
        Camisa cam = new Camisa(tam, descri, color, id, precio);
        return logica.registrarCamisa(cam);
    }// FIN DE REGISTAR CAMISA.

    /**
     * metodod usado para imprimir el arrayList de camisa.
     *
     * @return retorna la lista de camisas.
     */
    public String[] ListarCamisa() {
        return logica.getCamisa();
    }// FIN DE LISTAR CAMISA.


    /**
     * metodo usado para validar el cliente llamando la funcion que está en la capa logica.
     *
     * @param Pnombre:               variable de la clase Cliente de tipo String en el cual se guarda el nombre del cliente ingresado por el usuario.
     * @param Papellido1:            variable de la clase Cliente de tipo String en el cual se guarda el primer apellido del cliente ingresado por el usuario.
     * @param Papellido2:            variable de la clase Cliente de tipo String en el cual se guarda el segundo apellido  del cliente ingresado por el usuario.
     * @param PdirreccionExacta:dato referente a la  direción en donde el usuario desea recibir su pedido es de tipo String.
     * @param pcorreo:               variable de la clase Cliente en donde se almacena el correo de la persona quien hace el pedido, es de tipo String.
     * @return retorna la funcion de capa logica donde calcula si se repite o no.
     */
    public String validarCliente(String pcorreo, String Papellido1, String Papellido2, String PdirreccionExacta, String Pnombre) {
        return logica.validarCliente(pcorreo, Papellido1, Papellido2, PdirreccionExacta, Pnombre);
    }

    /**
     * metodod usado para imprimir el arrayList de cliente.
     *
     * @return retorna la lista de clientes.
     */
    public String[] ListarCliente() {
        return logica.getCliente();
    }// FIN DE LISTAR CLIENTE.


    public void agregarCamisa(String tam, String descri, String color, String id, double precio  ) {
        Camisa cam = new Camisa(tam,descri,color,id,precio);
        logica.registrarCamisa(cam);
    }
    public String agregarCamisaACatalogo(String iD, String id) {
        Catalogo cata = logica.averiguarCatalogo(iD);
        if (cata != null) {
            Camisa cami = logica.averiguarCamisa(id);
            if (cami != null) {
                cata.agregarCamisa(cami);
                return "Se agregó la camisa de manera correcta!";
            } else {
                return "El código de camisa: " + id + " no existe!";
            }
        }
        return "El catálogo indicado no existe";
    }
    public String agregarClienteACatalogo(String cedula, String iD) {
        Catalogo cata = logica.averiguarCatalogo(iD);
        if (cata != null) {
            Cliente cliente = logica.averiguarCliente(cedula);
            if (cliente != null) {
                cata.agregarCliente(cliente);
                return "Se agregó la camisa de manera correcta!";
            } else {
                return "El código de camisa: " + cedula + " no existe!";
            }
        }
        return "El catálogo indicado no existe";
    }
    public Camisa averiguarCamisa(String id) {
        return logica.averiguarCamisa(id);
    }
    public Cliente averiguarCliente(String cedula) {
        return logica.averiguarCliente(cedula);
    }
    public Catalogo averiguarCatalogo(String Id) {
        return logica.averiguarCatalogo(Id);
    }
    public String registrarCliente(String nombre, String apellido1, String apellido2, String correo, String dirreccionExacta, String cedula) {
        Cliente cl = new Cliente(nombre, apellido1, apellido2, correo, dirreccionExacta, cedula);
        return logica.registrarCliente(cl);
    }
    public int buscarPosCatalogo(String id) {
        int encontrada = logica.buscarPosCatalogo(id);
        return encontrada;
    }
    public String[] imprimirCatalogo() {
        return logica.imprimirCatalogo();
    }
    public String[] imprimirCamisa() {
        return logica.imprimirCamisa();
    }
    public String[] imprimirCliente() {
        return logica.imprimirCliente();
    }
}// FIN DE  CLASS