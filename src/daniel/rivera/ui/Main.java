package daniel.rivera.ui;

import daniel.rivera.bl.Cliente;
import daniel.rivera.tl.Controller;
import java.io.*;
import java.time.LocalDate;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Main {


    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller administrador = new Controller();

    public static void main(String[] args) throws IOException {
        mostrarMenu();
    }

    public static void crearMenu() throws IOException {
        int opcion = 0;
        do {
            out.println("1. Registrar Cliente");
            out.println("2. Listar Clientes");
            out.println("3. Registrar Producto");
            out.println("4. Listar Producto");
            out.println("5. Crear Factura");
            out.println("6. imprimir Facturas");
            out.println("7. Añadir linea a la factura");
            out.println("8. Salir");
            out.print("Digite la opcion: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 8);
    }

    public static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 1:
                registrarCliente();
                break;
            case 2:
                imprimirClientes();
                break;
            case 3:
                registrarCamisa();
                break;
            case 4:
                imprimirCamisas();
                break;
            case 5:
                registrarCatalogo();
                break;
            case 6:
                imprimirCatalogo();

                break;
            case 7:
                agregarClienteCatalogo();
                break;
            case 8:
                agregarCamisaCatalogo();
                break;
            case 9:
                out.println("Se ha cerrado correctamente");
                System.exit(0);
                break;
            default:
                out.println("valor registrado");
                break;
        }
    }
    static void mostrarMenu() throws IOException {
        int opcion = -1;
        do {
            System.out.println("Menú");
            System.out.println("1. Registrar un cliente");
            System.out.println("2. lista de clientes");
            System.out.println("");
            System.out.println("3. Registrar camisa");
            System.out.println("4. Listar camisa");
            System.out.println("");
            System.out.println("5. resgistrar catalogo");
            System.out.println("6. Lista de catalogos");
            System.out.println("");
            System.out.println("7. Asociar Cliente a catalogo");
            System.out.println("8. Asociar camisa a catalogo");
            System.out.println("");
            System.out.println("9. Salir");
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }
    static int seleccionarOpcion() throws IOException {
        System.out.println("Digite la opción");
        return Integer.parseInt(in.readLine());

    }
    public static void registrarCliente() throws IOException {
        Cliente client = new Cliente();
        out.println("Ingrese la cedula del cliente");
        String identificacion = in.readLine();
        out.println("Ingrese el nombre del cliente");
        String nombre = in.readLine();
        out.println("Ingrese el primer apellido  del cliente");
        String apellido1 = in.readLine();
        out.println("Ingrese el segundo apellido  del cliente");
        String apellido2 = in.readLine();
        out.println("Ingrese el correo del cliente");
        String correo = in.readLine();
        out.println("Ingrese la direccion exacta de donde quiere recibir su pedido");
        String dirreccionExacta = in.readLine();
        if (administrador.averiguarCliente(identificacion) == null) {
            String resultado = administrador.registrarCliente(nombre, apellido1, apellido2, dirreccionExacta,correo, identificacion);
            out.println(resultado);
        } else {
            out.println("El cliente con cedula: " + identificacion + " ya existe en el programa!");
        }

    }

    public static void registrarCamisa() throws IOException {
        out.println("Ingrese el id de la camisa");
        String id = in.readLine();
        out.println("Ingrese la descripcion del producto");
        String descripcion = in.readLine();
        out.println("Ingrese el tamaño de la camisa");
        String tam = in.readLine();
        out.println("Ingrese el color de la camisa");
        String color = in.readLine();
        out.println("Ingrese el precio de la camisa ");
        double precio = Double.parseDouble(in.readLine());
        if (administrador.averiguarCamisa(id) == null) {
            String resultado = administrador.registrarCamisa(tam,descripcion,color, id, precio);
            out.println(resultado);
        } else {
            out.println("La camisa con el id " + id + " ya existe en el registro !");
        }
    }

    public static void registrarCatalogo() throws IOException {
        out.println("Ingrese el id del catalogo");
        String id = in.readLine();
        out.println("Ingrese el dia de creacion del catalogo");
        int dia = Integer.parseInt(in.readLine());
        out.println("Ingrese el numero de mes de creacion del catalogo");
        int mes = Integer.parseInt(in.readLine());
        out.println("Ingrese nombre del mes de creacion del catalogo");
        String nombreDelMeS = in.readLine();
        out.println("Ingrese el año de creacion del catalogo");
        int year = Integer.parseInt(in.readLine());
        LocalDate fechaDeCreacion = LocalDate.of(year, mes, dia);
        if (administrador.averiguarCatalogo(id) == null) {
            String resultado = administrador.registrarCatalogo(id, nombreDelMeS, fechaDeCreacion);
            out.println(resultado);
        } else {
            out.println("El catalogo con id : " + id + " no existe!");
        }
        out.println("el catalogo se ha registrado correctamente");}


    public static void agregarCamisaCatalogo () throws IOException {
        System.out.print("Ingrese el id del catalogo: ");
        String id = in.readLine();
        System.out.print("Ingrese el id de la camisa  ya ingresado : ");
        String idCamisa = in.readLine();
        if (administrador.averiguarCatalogo(id) != null) {
            if (administrador.averiguarCamisa(idCamisa) != null) {
                String respuesta = administrador.agregarCamisaACatalogo(id,idCamisa);
            } else {
                out.println("La camisa con id " + id + " no se encuentra");
            }
        } else {
            out.println("El catalogo con id" + id + " no se encuentra");
        }
    }
    public static void agregarClienteCatalogo () throws IOException {
        System.out.print("Ingrese el id del catalogo: ");
        String id = in.readLine();
        out.println("Ingrese la cedula del cliente ");
        String cedula = in.readLine();
        if (administrador.averiguarCatalogo(id) != null) {
            if (administrador.averiguarCliente(cedula) != null) {
                String respuesta = administrador.agregarClienteACatalogo(cedula,id);
                out.println(respuesta);
            } else {
                out.println("El cliente con id " + id + " no se encuentra");
            }
        } else {
            out.println("El catalogo con id" + id + " no se encuentra");
        }
    }
    public static void imprimirCatalogo() throws IOException {
        System.out.print("Ingrese el número del catalogo que desea escoger: ");
        String id = in.readLine();
        int posCatalogo = administrador.buscarPosCatalogo(id);
        String[] datos = administrador.imprimirCatalogo();
        String datosFactura = datos[posCatalogo].toString();
        out.println(datosFactura + "\n");

    }


    public static void imprimirCamisas(){
        String[] datos = administrador.imprimirCamisa();
        for (String producto : datos) {
            out.println(producto);
        }
    }

    public static void imprimirClientes() {
        String[] datos = administrador.imprimirCliente();
        for (String factura : datos) {
            out.println(factura);
        }
    }
}

