package daniel.rivera.bl;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Cliente {
    
    private String Nombre, apellido1, apellido2, dirreccionExacta, correo,cedula;

    /**
     * Constructor vacio para la clase Cliente
     */
    public Cliente() {
    }// FIN DEL CONTRUCTOR POR DEFECTO.

    /**
     * Constructor que recibe todos los parámetros de  Cliente  y los Inicializa
     * @param nombre: variable de la clase Cliente de tipo String en el cual se guarda el nombre del cliente ingresado por el usuario.
     * @param apellido1: variable de la clase Cliente de tipo String en el cual se guarda el primer apellido del cliente ingresado por el usuario.
     * @param apellido2: variable de la clase Cliente de tipo String en el cual se guarda el segundo apellido  del cliente ingresado por el usuario.
     * @param dirreccionExacta:dato referente a la  direción en donde el usuario desea recibir su pedido es de tipo String.
     * @param correo: variable de la clase Cliente en donde se almacena el correo de la persona quien hace el pedido, es de tipo String.
     * @param cedula: variable donde se almacena la cedula del cliente
     */

    public Cliente(String nombre, String apellido1, String apellido2, String dirreccionExacta, String correo, String cedula) {
        this.Nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.dirreccionExacta = dirreccionExacta;
        this.correo = correo;
        this.cedula = cedula;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase Cliente
     * @return la variable de retorno simboliza el nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * Metodo utilizado para modificar el atributo privado nombre de la clase Cliente
     * @param nombre Variable que simboliza el nombre de la clase cliente
     */
    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada apellido de la clase Cliente
     * @return
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * Metodo utilizado para modificar el atributo privado apellido de la clase Cliente
     * @param apellido1 Variable que simboliza el apellido de la clase cliente
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada apellido de la clase
     * @return la variable de retorno simboliza el apellido
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Apellido de la clase Cliente
     * @param apellido2 Variable que simboliza apellido de la clase Cliente
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada direccion exacta de la clase Cliente
     * @return la variable de retorno simboliza la direccion del cliente
     */
    public String getDirreccionExacta() {
        return dirreccionExacta;
    }

    /**
     * Metodo utilizado para modificar el atributo privado direccion exacta de la clase Cliente
     * @param dirreccionExacta
     */
    public void setDirreccionExacta(String dirreccionExacta) {
        this.dirreccionExacta = dirreccionExacta;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada correo de la clase Cliente
     * @return la variable de retorno simboliza el correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Correo de la clase Cliente
     * @param correo Variable que simboliza el correo de la clase
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada cedula de la clase Cliente
     * @return la variable de retorno simboliza la cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * Metodo utilizado para modificar el atributo privado cedula de la clase cliente
     * @param cedula Variable que simboliza la cedula de la clase cliente
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
         * Metodo utilizado para imprimir todos los atributos de la clase - en un unico String
         * @return la variable de retorno simboliza todos los valores de la clase - en unico string
         */
    public String toString() {
        return "Cliente{" +
                "Nombre='" + Nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", dirreccionExacta='" + dirreccionExacta + '\'' +
                ", correo='" + correo + '\'' +
                '}';
    }


}// FIN DE CLAs
