package daniel.rivera.bl;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Camisa {
    //Atributos
    private String tam, descri, color, id;
    private double precio;


    /**
     * Contructor por defecto de la clase Camisa y puede usarse para hacer casos de prueba o invocación de subclases.
     */
    public Camisa() {
    }// FIN DE CONSTRUCTOR POR DEFECTO

    /**
     * Constructor que recibe todos los parámetros, evita que algún valor se almacene por defecto osea con el valor de "null" en Strings o "0" en enteros...
     *
     * @param tam:    dato de la clase Camisa de tipo String en el cual se guara el tamaño de la creación de la camisa ingresado por el usuario.
     * @param color:  variable en donde se guarda el color de la camisa deseada por el cliente.
     * @param id:dato identificativo de la camisa, hace referencia a un codigo único de la mismo.
     * @param descri: variable que describe detalladamente las caracteristicas de la camisa.
     * @param precio: variable que contiene el precio despectivo de la camisa en especifico ingresado por el usuario.
     */

    public Camisa(String tam, String descri, String color, String id, double precio ) {
        this.tam = tam;
        this.descri = descri;
        this.color = color;
        this.id = id;
        this.precio=precio;

    }

    /**
     * Metódo Gettter de la variable tamaño, es utilizado para obterner el atributo privado de una clase.
     *
     * @return devuelve del atributo que almacena el tamaño  de la camisa ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getTam() {
        return tam;
    }

    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase, en este caso al tamaño de la camisa.
     *
     * @param tam variable que almacena el tamaño de la camisa ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public void setTam(String tam) {
        this.tam = tam;
    }

    /**
     * Metódo Gettter de la variable descripcion, es utilizado para obterner el atributo privado de una clase.
     *
     * @return devuelve del atributo que almacena la descripcion de la camisa ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */

    public String getDescri() {
        return descri;
    }

    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase, en este caso al tamaño de la camisa.
     *
     * @param descri: variable que almacena la descripción de la camisa ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setDescri(String descri) {
        this.descri = descri;
    }

    /**
     * Metódo Gettter de la variable color, es utilizado para obterner el atributo privado de una clase.
     *
     * @return devuelve del atributo que almacena el color de la camisa ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */


    public String getColor() {
        return color;
    }

    /**
     * Metódo Gettter de la variable id, es utilizado para obterner el atributo privado de una clase.
     *
     * @return devuelve el codigo del atributo que almacena id de la camisa ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getid() {
        return id;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase, en este caso al id de la camisa.
     * @param id variable que almacena el codigo de la camisa ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase, en este caso al color de la camisa.
     *
     * @param color: variable que almacena el codigo del color de la camisa ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Precio de la clase Camisa
     * @return la variable de retorno simboliza el precio de la camisa
     *
     */
    public double getPrecio() {
        return precio;
    }

    /**
     *
     * @param precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    /**
     * Metódo Gettter de la variable precio, es utilizado para obterner el atributo privado de una clase.
     *
     * @return devuelve el precio de la camisa ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    /**
     * Método toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase Camisa en un solo String.
     */
    @Override
    public String toString() {
        return "Camisa{" +
                "tam='" + tam + '\'' +
                ", descri='" + descri + '\'' +
                ", color='" + color + '\'' +
                ", id='" + id + '\'' +
                ", precio='" + precio + '\'' +
                '}';
    }
}// FIN DE LA CLASE.